.. _chap:page:

Pages and Page Objects
======================

.. _pages_and_page_objects:

.. rubric:: Creating an end-user interface

After you have created a model in AIMMS to represent and solve a
particular problem, you may want to move on to the next step: creating a
graphical end-user interface around the model. In this way, you and your
end-users are freed from having to enter (or alter) the model data in
text or database tables. Instead, they can make the necessary
modifications in a graphical environment that best suits the purposes of
your model. Similarly, using the advanced graphical objects available in
AIMMS (such as the Gantt chart and network flow object), you can present
your model results in an intuitive manner, which will help your
end-users interpret a solution quickly and easily.



This chapter gives you an overview of the possibilities that AIMMS
offers you for creating a complete model-based end-user application. It
describes pages, which are the basic medium in AIMMS for displaying
model input and output in a graphical manner. In addition, the chapter
illustrates how page objects (which provide a graphical display of one
or more identifiers in your model) can be created and linked together.

.. toctree::

   introduction
   creating-pages
   adding-page-objects
   selecting-identifier-slices-and-linking-objects
