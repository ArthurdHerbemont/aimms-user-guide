About AIMMS
===========

.. rubric:: History

AIMMS was introduced as a new type of mathematical modeling tool in
1993-an integrated combination of a modeling language, a graphical user
interface, and numerical solvers. AIMMS has proven to be one of the
world's most advanced development environments for building
optimization-based decision support applications and advanced planning
systems. Today, it is used by leading companies in a wide range of
industries in areas such as supply chain management, energy management,
production planning, logistics, forestry planning, and risk-, revenue-,
and asset- management. In addition, AIMMS is used by universities
worldwide for courses in Operations Research and Optimization Modeling,
as well as for research and graduation projects.

.. rubric:: What is AIMMS?

AIMMS is far more than just another mathematical modeling language.
True, the modeling language is state of the art for sure, but alongside
this, AIMMS offers a number of advanced modeling concepts not found in
other languages, as well as a full graphical user interface both for
developers and end-users. AIMMS includes world-class solvers (and solver
links) for linear, mixed-integer, and nonlinear programming such as
BARON, CPLEX, conopt, GUROBI, KNITRO, PATH, SNOPT and XA, and can be
readily extended to incorporate other advanced commercial solvers
available on the market today. In addition, concepts as stochastic
programming and robust optimization are available to include data
uncertainty in your models.

.. rubric:: Mastering AIMMS

Mastering AIMMS is straightforward since the language concepts will be
intuitive to Operations Research (OR) professionals, and the
point-and-click graphical interface is easy to use. AIMMS comes with
comprehensive documentation, available electronically and in book form.

.. rubric:: Types of AIMMS applications

AIMMS provides an ideal platform for creating advanced prototypes that
are then easily transformed into operational end-user systems. Such
systems can than be used either as

-  stand-alone applications, or

-  optimization components.

.. rubric:: Stand-alone applications

Application developers and operations research experts use AIMMS to
build complex and large scale optimization models and to create a
graphical end-user interface around the model. AIMMS-based applications
place the power of the most advanced mathematical modeling techniques
directly into the hands of end-users, enabling them to rapidly improve
the quality, service, profitability, and responsiveness of their
operations.

.. rubric:: Optimization components

Independent Software Vendors and OEMs use AIMMS to create complex and
large scale optimization components that complement their applications
and web services developed in languages such as ``C++``, Java, .NET,
or Excel. Applications built with AIMMS-based optimization components
have a shorter time-to-market, are more robust and are richer in
features than would be possible through direct programming alone.

.. rubric:: AIMMS users

Companies using AIMMS include

-  ABN AMRO

-  Areva

-  Bayer

-  Bluescope Steel

-  BP

-  CST

-  ExxonMobil

-  Gaz de France

-  Heineken

-  Innovene

-  Lufthansa

-  Merck

-  Owens Corning

-  Perdigão

-  Petrobras

-  Philips

-  PriceWaterhouseCoopers

-  Reliance

-  Repsol

-  Shell

-  Statoil

-  Unilever

Universities using AIMMS include Budapest University of Technology,
Carnegie Mellon University, George Mason University, Georgia Institute
of Technology, Japan Advanced Institute of Science and Technology,
London School of Economics, Nanyang Technological University, Rutgers
University, Technical University of Eindhoven, Technische Universität
Berlin, UIC Bioengineering, Universidade Federal do Rio de Janeiro,
University of Groningen, University of Pittsburgh, University of Warsaw,
and University of the West of England.

A more detailed list of AIMMS users and reference cases can be found on
`our website <http://www.aimms.com>`__.