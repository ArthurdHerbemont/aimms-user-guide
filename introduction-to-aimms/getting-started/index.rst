.. _chap:start:

Getting Started
===============



This chapter provides pointers to the various AIMMS examples that are
available online and may help you to get a quick feel for the system. It
explains the principle steps involved in creating a new AIMMS project,
and it provides you with an overview of the various graphical tools
available in AIMMS to help you create a complete Analytic Decision
Support application. In addition, the chapter discusses the files
related to an AIMMS project.

.. toctree::

   getting-started-with-aimms
   creating-a-new-project
   modeling-tools
   dockable-windows
   additional-files-related-to-an-aimms-project
   aimms-licensing
